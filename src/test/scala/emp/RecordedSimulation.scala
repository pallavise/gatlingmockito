
import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class RecordedSimulation extends Simulation {

	val httpProtocol = http
		.baseURL("http://172.27.59.147:8088")
		.inferHtmlResources()



    val uri1 = "http://172.27.59.147:8088"
    val uri2 = "ocsp.digicert.com"

	val scn = scenario("RecordedSimulation")
		.exec(http("request_0")
			.get("/MockitoPOC")
			.resources(http("request_1")
			.get(uri1 + "/favicon.ico")
			.check(status.is(404)),
            http("request_2")
			.get(uri1 + "/favicon.ico")
			.check(status.is(404))))
		.pause(21)
		.exec(http("request_3")
			.post("http://" + uri2 + "/")
			.body(RawFileBody("RecordedSimulation_0003_request.txt")))
		.pause(46)
		.exec(http("request_4")
			.get("/MockitoPOC/hello"))

	setUp(scn.inject(atOnceUsers(1))).protocols(httpProtocol)
}